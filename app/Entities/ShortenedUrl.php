<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model;

class ShortenedUrl extends Model
{
    /**
     * Overriding the table name
     * @var string
     */
    protected $table = 'shortened_urls';

    protected $fillable = [
        'slug',
        'long_url',
    ];

}
