<?php

namespace App\Entities;

use Jenssegers\Mongodb\Eloquent\Model;

class Counter extends Model
{
    /**
     * Overriding the table name
     * @var string
     */
    protected $table = 'counter';

    protected $fillable = [
        'name',
        'value'
    ];
}
