<?php

namespace App\Models;

use JetBrains\PhpStorm\ArrayShape;

class ShortenedUrl
{
    /** Expanded URL
     * @var string
     */
    private string $longUrl;

    /** Shorten id (slug) of shortened URL
     * @var string
     */
    private string $slug;

    /** Shorted absolute URL
     * @var string
     */
    private string $shortUrl;

    /**
     * ShortenedUrl Model constructor
     * @param string $longUrl
     * @param string $slug
     * @param string $shortUrl
     */
    public function __construct(string $longUrl, string $slug, string $shortUrl)
    {
        $this->longUrl = $longUrl;
        $this->slug = $slug;
        $this->shortUrl = $shortUrl;
    }

    /** Returns the long URL
     * @return string
     */
    public function getLongUrl(): string
    {
        return $this->longUrl;
    }

    /** Returns the absolute short URL
     * @return string
     */
    public function getShortUrl(): string
    {
        return $this->shortUrl;
    }

    /** Returns the unique slug
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /** Get all the model properties as array
     * @return array
     */
    #[ArrayShape(['long_url' => "string", 'slug' => "string", 'short_url' => "string"])]
    public function getData(): array
    {
        return [
            'long_url' => $this->longUrl,
            'slug' => $this->slug,
            'short_url' => $this->shortUrl
        ];
    }
}
