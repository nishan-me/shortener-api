<?php

namespace App\Events;

use App\Models\ShortenedUrl;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UrlShortened
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public ShortenedUrl $shortenedUrl;

    /**
     * UrlShortened constructor.
     * @param ShortenedUrl $shortenedUrl
     */
    public function __construct(ShortenedUrl $shortenedUrl)
    {
        $this->shortenedUrl = $shortenedUrl;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
