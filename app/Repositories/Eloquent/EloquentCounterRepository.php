<?php namespace App\Repositories\Eloquent;

use App\Entities\Counter;
use App\Repositories\Contracts\CounterRepositoryInterface;

class EloquentCounterRepository extends EloquentRepository implements CounterRepositoryInterface
{

    /** Entity used in the repository
     * @return string
     */
    function model(): string
    {
        return Counter::class;
    }

}
