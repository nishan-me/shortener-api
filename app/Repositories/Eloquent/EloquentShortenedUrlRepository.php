<?php namespace App\Repositories\Eloquent;

use App\Entities\ShortenedUrl;
use App\Repositories\Contracts\ShortenedUrlRepositoryInterface;

class EloquentShortenedUrlRepository extends EloquentRepository implements ShortenedUrlRepositoryInterface
{

    /** Entity used in the repository
     * @return string
     */
    function model(): string
    {
        return ShortenedUrl::class;
    }

}
