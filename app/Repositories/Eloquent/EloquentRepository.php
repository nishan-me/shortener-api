<?php

namespace App\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Container\Container as App;
use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Exceptions\RepositoryException;
use Illuminate\Contracts\Container\BindingResolutionException;


abstract class EloquentRepository implements RepositoryInterface
{

    /** Container Instance to build dependencies
     * @var App|null
     */
    private ?App $app;

    /** Concrete dependency
     * @var Builder|null
     */
    protected ?Builder $model;

    /**
     * EloquentRepository constructor.
     * @param App $app
     * @throws RepositoryException|BindingResolutionException
     */
    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    /**
     * Specify Model class name
     * @return string
     */
    public abstract function model(): string;

    /**
     * Get all records
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*')): mixed
    {
        return $this->model->get($columns);
    }

    /**
     * Get the first record
     * @param array $columns
     * @return mixed
     */
    public function first($columns = array('*')): mixed
    {
        return $this->model->first($columns);
    }

    /**
     * Paginate the result set
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 1, $columns = array('*')): mixed
    {
        return $this->model->paginate($perPage, $columns);
    }

    /**
     * Persist the data
     * @param array $data
     * @return mixed
     */
    public function create(array $data): mixed
    {
        return $this->model->create($data);
    }

    /**
     * Update data
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id"): mixed
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    /**
     * Delete a record by ID
     * @param $id
     * @return mixed
     */
    public function delete($id): mixed
    {
        return $this->model->destroy($id);
    }

    /**
     * Search for a record by ID
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*')): mixed
    {
        return $this->model->find($id, $columns);
    }

    /**
     * Search for a record
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')): mixed
    {
        return $this->model->where($attribute, '=', $value)->first($columns);
    }

    /**
     * Resolve dependencies
     * @return Builder
     * @throws RepositoryException
     * @throws BindingResolutionException
     */
    public function makeModel(): Builder
    {
        $model = $this->app->make($this->model());

        if (!$model instanceof Model) {
            throw new RepositoryException("Class {$this->model()} must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }

        return $this->model = $model->newQuery();
    }

}
