<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Exceptions\RepositoryException;

interface  RepositoryInterface
{
    /** Specify Model class name
     * @return string
     */
    public function model(): string;

    /**
     * @param array $columns
     * @return mixed
     */
    public function all($columns = array('*')): mixed;

    /**
     * @param array $columns
     * @return mixed
     */
    public function first($columns = array('*')): mixed;

    /**
     * @param int $perPage
     * @param array $columns
     * @return mixed
     */
    public function paginate($perPage = 1, $columns = array('*')): mixed;

    /**
     * @param array $data
     * @return mixed
     */
    public function create(array $data): mixed;

    /**
     * @param array $data
     * @param $id
     * @param string $attribute
     * @return mixed
     */
    public function update(array $data, $id, $attribute = "id"): mixed;

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id): mixed;

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*')): mixed;

    /**
     * @param $attribute
     * @param $value
     * @param array $columns
     * @return mixed
     */
    public function findBy($attribute, $value, $columns = array('*')): mixed;

    /**
     * @return Builder
     * @throws RepositoryException
     */
    public function makeModel(): Builder;
}
