<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Eloquent\EloquentShortenedUrlRepository;
use App\Repositories\Contracts\ShortenedUrlRepositoryInterface;
use App\Repositories\Contracts\CounterRepositoryInterface;
use App\Repositories\Eloquent\EloquentCounterRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register any repository services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ShortenedUrlRepositoryInterface::class, EloquentShortenedUrlRepository::class);
        $this->app->bind(CounterRepositoryInterface::class, EloquentCounterRepository::class);
    }
}
