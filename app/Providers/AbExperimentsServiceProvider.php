<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\AbExperiments\LocalAbExperiments;
use App\Services\AbExperiments\Contracts\AbExperimentsInterface;

class AbExperimentsServiceProvider extends ServiceProvider
{
    /**
     * Register any repository services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AbExperimentsInterface::class, LocalAbExperiments::class);
    }
}
