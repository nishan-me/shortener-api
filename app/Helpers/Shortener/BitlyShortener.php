<?php

namespace App\Helpers\Shortener;

use GuzzleHttp\Client;
use App\Models\ShortenedUrl;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Exception\GuzzleException;
use App\Helpers\Shortener\Contracts\ShortenerInterface;

class BitlyShortener extends Shortener implements ShortenerInterface
{

    /** Holds a instance of Guzzle client
     * @var Client|null
     */
    private ?Client $client = null;

    /** Set the long URL to be shortened
     * @param string $longUrl
     * @return $this
     */
    public function setLongUrl(string $longUrl): self
    {
        $this->longUrl = $longUrl;
        return $this;
    }

    /** Create the slug for long URL and responds with the ShortenedUrl model
     * @return ShortenedUrl
     */
    public function shorten(): ShortenedUrl
    {
        $response = $this->makePost('/shorten', ['long_url' => $this->longUrl]);
        return new ShortenedUrl(
            $this->longUrl,
            $this->extractSlugFromBitlyId($response->id),
            $response->link
        );
    }

    /** Instantiate or responds with the current instance of a Guzzle client
     * @return Client
     */
    private function getClient(): Client
    {
        if (!$this->client) {
            $this->client = new Client([
                'base_uri' => env('BITLY_BASE_URL')
            ]);
        }
        return $this->client;
    }

    /** Make a client POST request
     * @param string $endpoint
     * @param array $payload
     * @return false|object
     */
    private function makePost(string $endpoint, array $payload)
    {
        try {
            $response = $this->getClient()->post(env('BITLY_API_VERSION') . $endpoint, [
                RequestOptions::JSON => $payload,
                RequestOptions::HEADERS => ['Authorization' => 'Bearer ' . env('BITLY_ACCESS_TOKEN')]
            ]);

            return json_decode($response->getBody());
        } catch (GuzzleException $e) {
            //TODO Handle error
            return false;
        }

    }

    /** Bitly doesn't provide the slug separately in the response, extract it from the id
     * @param string $id
     * @return mixed|string
     */
    private function extractSlugFromBitlyId(string $id)
    {
        return explode('/', $id)[1];
    }
}
