<?php

namespace App\Helpers\Shortener;

abstract class Shortener
{
    /** URL to be shortened/ Expanded URL
     * @var string
     */
    protected string $longUrl;

    /** Unique slug created for the long URL
     * @var string
     */
    protected string $slug;

}
