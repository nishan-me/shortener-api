<?php

namespace App\Helpers\Shortener;

use App\Models\ShortenedUrl;
use App\Utils\Slug\RandomBytesSlug;
use App\Helpers\Shortener\Contracts\ShortenerInterface;

class LocalShortener extends Shortener implements ShortenerInterface
{
    /** Set the long URL to be shortened
     * @param string $longUrl
     * @return $this
     */
    public function setLongUrl(string $longUrl): self
    {
        $this->longUrl = $longUrl;
        return $this;
    }

    /** Create the slug for long URL and responds with the ShortenedUrl model
     * @return ShortenedUrl
     */
    public function shorten(): ShortenedUrl
    {
        $slugGenerator = new RandomBytesSlug();
        $slug = $slugGenerator->generateSlug(10);

        return new ShortenedUrl(
            $this->longUrl,
            $slug,
            $this->getShortUrl($slug)
        );
    }

    /** Builds the absolute short URL
     * @param string $slug
     * @return string
     */
    protected function getShortUrl(string $slug): string
    {
        return env('APP_URL') . '/' . $slug;
    }

}
