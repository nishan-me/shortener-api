<?php

namespace App\Helpers\Shortener\Contracts;

use App\Models\ShortenedUrl;

interface ShortenerInterface
{
    /** Set the long URL to be shortened
     * @param string $longUrl
     * @return $this
     */
    public function setLongUrl(string $longUrl) : self;

    /** Create the slug for long URL and responds with the ShortenedUrl model
     * @return ShortenedUrl
     */
    public function shorten() : ShortenedUrl;
}
