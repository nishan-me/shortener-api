<?php

namespace App\Helpers;

use App\Helpers\Shortener\Contracts\ShortenerInterface;

class ShortenerFactory
{
    /** Builds the shortener based on the current strategy for shortening the URL
     * @param string $type
     * @return ShortenerInterface
     * @throws \Exception
     */
    public static function build($type)
    {
        $shortener = sprintf('\\App\\Helpers\\Shortener\\%sShortener', ucfirst($type));
        if (class_exists($shortener)) {
            return new $shortener();
        } else {
            throw new \Exception('Shortener helper not found.');
        }
    }
}
