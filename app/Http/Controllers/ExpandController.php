<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Repositories\Contracts\ShortenedUrlRepositoryInterface;

class ExpandController
{
    private ShortenedUrlRepositoryInterface $shortenedUrl;

    /**
     * ExpandController constructor.
     * @param ShortenedUrlRepositoryInterface $shortenedUrl
     */
    public function __construct(ShortenedUrlRepositoryInterface $shortenedUrl)
    {
        $this->shortenedUrl = $shortenedUrl;
    }

    /**
     * Accepts the slug, validate of the slug exists
     * Redirect to the long url if found
     * @param Request $request
     * @param string $slug
     * @return RedirectResponse
     */
    public function expandSlug(Request $request, string $slug): RedirectResponse
    {
        try {
            //See if the slug exist in local records
            $savedUrl = $this->shortenedUrl->findBy('slug', $slug);

            //If exists redirect to the long url
            if ($savedUrl) {
                return redirect($savedUrl->long_url);
            }

            abort(404);
        } catch (\Exception $e) {
            abort(404);
        }
    }
}
