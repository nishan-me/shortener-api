<?php

namespace App\Http\Controllers\Api;

use App\Events\UrlShortened;
use Illuminate\Http\Request;
use App\Http\Traits\ApiResponse;
use App\Helpers\ShortenerFactory;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;
use App\Repositories\Contracts\ShortenedUrlRepositoryInterface;
use App\Services\AbExperiments\Contracts\AbExperimentsInterface;

class ShortenController extends Controller
{
    use ApiResponse;

    private ShortenedUrlRepositoryInterface $shortenedUrl;

    private AbexperimentsInterface $abExperiments;

    /**
     * ShortenController constructor.
     * @param ShortenedUrlRepositoryInterface $shortenedUrl
     * @param AbExperimentsInterface $abExperiments
     */
    public function __construct(ShortenedUrlRepositoryInterface $shortenedUrl, AbExperimentsInterface $abExperiments)
    {
        $this->shortenedUrl = $shortenedUrl;
        $this->abExperiments = $abExperiments;
    }

    public function createSlug(Request $request): JsonResponse
    {
        try {
            $request->validate([
                'long_url' => 'required|url'
            ]);

            $shortenStrategy = $this->abExperiments->getExperiment('localvsbitly')->next();

            //Shorten the URL and receives the common model
            $shortener = ShortenerFactory::build($shortenStrategy);
            $shortenedUrl = $shortener->setLongUrl($request->get('long_url'))->shorten();

            //Save the created slug, if using local
            //TODO Make this LocalShortener's responsibility
            if ($shortenStrategy == 'local') {
                $createdEntry = $this->shortenedUrl->create($shortenedUrl->getData());
            }

            //Dispatch a new event, can be used to increment counter, analytics
            event(new UrlShortened($shortenedUrl));

            //Respond with short URL
            return $this->jsonResponse(
                'OK',
                $shortenedUrl->getData()
            );

        } catch (ValidationException $e) {
            return $this->jsonResponse($e->getMessage(), $e->validator->getMessageBag(), false, 400);
        } catch (\Exception $e) {
            return $this->jsonResponse('Error creating shortened url', $e->getMessage(), false,
                400);
        }
    }
}
