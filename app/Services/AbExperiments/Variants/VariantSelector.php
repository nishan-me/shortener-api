<?php

namespace App\Services\AbExperiments\Variants;

class VariantSelector
{
    /** Current stickiness counter
     * @var int
     */
    private int $counter;

    /** Available variants
     * @var array
     */
    private array $variants;

    /**
     * VariantSelector constructor.
     * @param int $counter
     * @param array $variants
     */
    public function __construct(int $counter, array $variants)
    {
        $this->counter = $counter;
        $this->variants = $variants;
    }

    /** Get the active variant for the current counter
     * @return array
     */
    public function getActiveVariant(): array
    {
        $variantsDistribution = [];
        $variantsWeight = [];

        foreach ($this->variants as $variant) {
            $variantsWeight[$variant['index']] = $variant['participants_percentage'];
        }

        // define variant for each position
        for ($i = 0; $i < 100; $i++) {

            //Original behavior, ie. not in experiment
            $variant = array(
                'index' => -1,
                'weight' => 0
            );

            foreach ($variantsWeight as $variantIndex => $variantWeight) {
                if ($variantWeight > $variant['weight']) {
                    $variant['index'] = $variantIndex;
                    $variant['weight'] = $variantWeight;
                }
            }

            if ($variant['index'] >= 0) {
                $variantsWeight[$variant['index']]--;
            }

            $variantsDistribution[$i] = $variant['index'];
        }

        $counterPosition = $this->counter % 100;
        return $this->variants[$variantsDistribution[$counterPosition]];
    }
}
