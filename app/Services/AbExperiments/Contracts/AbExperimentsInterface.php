<?php

namespace App\Services\AbExperiments\Contracts;

use App\Services\AbExperiments\LocalAbExperiments;

interface AbExperimentsInterface
{
    /** Extract the selected experiment from current experiments
     * @param string $experimentCode
     * @return ?self
     */
    public function getExperiment(string $experimentCode): ?self;

    /** Returns the next variant of the currently selected experiment
     * @return string
     */
    public function next(): string;
}
