<?php

namespace App\Services\AbExperiments;

use App\Services\AbExperiments\Variants\VariantSelector;
use App\Repositories\Contracts\CounterRepositoryInterface;
use App\Services\AbExperiments\Contracts\AbExperimentsInterface;

class LocalAbExperiments implements AbExperimentsInterface
{
    /** Holds the experiment in the current instance
     * @var array
     */
    private array $experiment;

    /** Holds all valid experiments
     * @var array
     */
    private array $activeAbExperiments = [];

    /** Repository to be used when the stickiness is set to counter
     * @var CounterRepositoryInterface
     */
    private CounterRepositoryInterface $counterRepository;

    /**
     * LocalAbExperiments constructor.
     * @param CounterRepositoryInterface $counterRepository
     */
    public function __construct(CounterRepositoryInterface $counterRepository)
    {
        $this->activeAbExperiments = $this->validate(config('abexperiments'));
        $this->counterRepository = $counterRepository;
    }

    /** Validates the experiment config and set the valid experiments for current instance
     * @param array $experiments
     * @return array
     */
    private function validate(array $experiments): array
    {
        $validExperiments = [];

        foreach ($experiments as $experiment) {
            if (strtotime($experiment['start_date']) < time() && strtotime($experiment['end_date']) > time()) {
                $validExperiments[$experiment['code']] = $experiment;
            }
        }
        return $validExperiments;
    }

    /** Extract the selected experiment from current experiments
     * @param string $experimentCode
     * @return $this|null
     */
    public function getExperiment(string $experimentCode): ?self
    {
        try {
            $this->experiment = $this->activeAbExperiments[$experimentCode];
        } catch (\Exception $e) {
            return null;
        }
        return $this;
    }

    /** Returns the next variant of the currently selected experiment
     * @return string
     */
    public function next(): string
    {
        //Get the next stickiness counter
        $stickiness = $this->experiment['stickiness'];
        $stickinessColumn = $this->experiment['stickiness_column'];
        $counter = $this->getCurrentStickinessCounter($stickiness, $stickinessColumn);

        //Select the next variant
        $variantSelector = new VariantSelector($counter, $this->experiment['variants']);
        $activeVariant = $variantSelector->getActiveVariant();
        return $activeVariant['code'];
    }

    /** Extract the current counter from the stickiness field provided
     * @param string $stickiness
     * @param string $stickinessColumn
     * @return int
     */
    private function getCurrentStickinessCounter(string $stickiness, string $stickinessColumn): int
    {

        //TODO Needs to be offloaded to a Factory
        //If the stickiness if set to user id, don't increment the counter
        switch ($stickiness) {
            case 'counter':
            default:
                $currentCounter = $this->counterRepository->findBy('name', $stickinessColumn)->value;
                $counter = $currentCounter + 1;
                break;
        }

        return $counter;
    }



}
