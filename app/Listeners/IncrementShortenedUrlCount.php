<?php

namespace App\Listeners;

use App\Events\UrlShortened;
use App\Repositories\Contracts\CounterRepositoryInterface;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class IncrementShortenedUrlCount
{
    private CounterRepositoryInterface $counterRepository;

    /**
     * IncrementShortenedUrlCount constructor.
     * @param CounterRepositoryInterface $counterRepository
     */
    public function __construct(CounterRepositoryInterface $counterRepository)
    {
        $this->counterRepository = $counterRepository;
    }

    /**
     * Handle the event.
     *
     * @param UrlShortened $event
     * @return void
     */
    public function handle(UrlShortened $event)
    {
        // Get the current count
        $counterEntry = $this->counterRepository->findBy('name', 'shortened_url_count');
        if ($counterEntry) {
            $counter = $counterEntry->value + 1;

            //Update the counter
            $counterEntry->update([
                'value' => $counter
            ]);
        } else {
            //Create a new counter if the counter does not exist
            $this->counterRepository->create([
                'name' => 'shortened_url_count',
                'value' => 1
            ]);
        }
    }
}
