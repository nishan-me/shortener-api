<?php

namespace App\Utils\Slug\Contracts;

interface SlugInterface
{
    /** Generate a unique key for the given length
     * @param int $length
     * @return string
     */
    public function generateSlug(int $length): string;
}
