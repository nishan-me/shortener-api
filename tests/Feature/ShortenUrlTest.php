<?php

use Tests\TestCase;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ShortenUrlTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     * @group shortner
     */
    public function any_user_can_request_a_shortened_url()
    {
        $shortenRequestData = $this->templateShortenUrlRequest();
        $response = $this->post('/api/shorten', $shortenRequestData);
        $response->assertJsonStructure([
            'message',
            'status_code',
            'data' => [
                'short_url',
                'slug',
                'long_url',
            ]
        ])
            ->assertStatus(200);

        $this->assertDatabaseHas('shortened_urls', ['slug' => $response['data']['slug']]);
    }

    /**
     * @test
     * @group shortner
     */
    public function long_url_is_required_to_create_a_short_url()
    {
        $shortenRequestData = $this->templateShortenUrlRequest();
        unset($shortenRequestData['long_url']);
        $response = $this->post('/api/shorten', $shortenRequestData);
        $response->assertJsonStructure([
            'message',
            'errors'
        ])
            ->assertStatus(400);
    }

    /**
     * Return sample support ticket for testing
     * @return array
     */
    #[ArrayShape(['long_url' => "string"])]
    protected function templateShortenUrlRequest(): array
    {
        $data = [
            'long_url' => 'https://www.werkspot.nl/post-service-request/dakterras-aanleggen'
        ];
        return $data;
    }
}
