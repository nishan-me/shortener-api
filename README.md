# URL Shortener

## Running the application

Clone the repository and run the following commends inside the project directory
```
> cp .env.example .env
> composer install
> ./vendor/bin/sail up
> ./vendor/bin/sail php artisan migrate
```

#### Please add a bitly access key to .env make use of the bitly API
BITLY_ACCESS_TOKEN=

API is ruuning on [http://localhost](http://localhost) 

Feature Tests can be run by using the following command
```
> ./vendor/bin/sail php artisan test
```
