<?php

return [
    [
        'code' => 'localvsbitly',
        'start_date' => '10-04-2021',
        'end_date' => '10-04-2022',
        'stickiness' => 'counter',
        'stickiness_column' => 'shortened_url_count',
        'variants' => [
            [
                'index' => 0,
                'code' => 'local',
                'participants_percentage' => 30
            ],
            [
                'index' => 1,
                'code' => 'bitly',
                'participants_percentage' => 70
            ]
        ]
    ]
];
